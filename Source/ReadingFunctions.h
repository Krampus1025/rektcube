#include <Windows.h>
#include <cmath>

const float pi = 3.14159265358979f;

//Contains information about the game window
struct Window
{
	int * Width;
	int * Height;
	DWORD * WidthAddress;
	DWORD * HeightAddress;

	//Grabs the window Width and Height directly from AssaultCube
	void GrabInfo()
	{
		WidthAddress = (DWORD*)0x510C94;
		HeightAddress = (DWORD*)0x510C98;

		Width = (int*)WidthAddress;
		Height = (int*)HeightAddress;

		
	}
}windowz;

//Returns the 3D distance between you and another entity
float Get3dDistance(float * myCoords, float * enemyCoords)
{
	return sqrt(
		pow(double(enemyCoords[0] - myCoords[0]), 2.0) +
		pow(double(enemyCoords[1] - myCoords[1]), 2.0) +
		pow(double(enemyCoords[2] - myCoords[2]), 2.0));
}

//Contains your information
struct MyPlayer
{
	const int * NumOfPlayers;
	const int * OfflineNumOfPlayers;

	DWORD Address;
	int * Health;
	int * GameMode;
	BYTE * MouseButton;
	const int * Team;
	float * Position[3];
	float * ViewAngles[2];
	char * CrosshairID[15];

	//Read your Health, Team Number, View Angles,
	//the Game Mode, and the Number Of Players
	void ReadInformation()
	{
		//Finding And Assigning The Health Number
		Address = *(DWORD*)0x509B74 + 0xF8;
		Health = (int*)Address;

		//Finding And Assigning The Health Number
		Address = *(DWORD*)0x509B74 + 0x224;
		MouseButton = (BYTE*)Address;

		//Finding And Assigning The GameMode
		GameMode = (int*)0x50F49C;

		//Finding And Assigning The CrosshairID
		for (int i = 0; i <= 15; i++)
		{
			CrosshairID[i] = (char*)0x501C38 + i;
		}

		//Finding And Assigning The Team ID
		Address = *(DWORD*)0x509B74 + 0x32C;
		Team = (int*)Address;

		Address = *(DWORD*)0x509B74 + 0x40;
		ViewAngles[0] = (float*)Address;

		Address = *(DWORD*)0x509B74 + 0x44;
		ViewAngles[1] = (float*)Address;

		//Finding And Assigning The Online Number Of Players
		NumOfPlayers = (int*)0x50A22C;

		//Finding And Assigning The Offline Number Of Players
		OfflineNumOfPlayers = (int*)0x510D98;
	}

	//Read your Position
	void ReadPosition()
	{
		int j = 0;
		for (int i = 0x4; i <= 0xC; i += 0x4)
		{
			Address = *(DWORD*)0x509B74 + i;
			Position[j] = (float*)Address;
			j++;
		}
	}
}MyPlayer;

//The online entity list
struct PlayerList_t
{
	float ThreeDdistance;
	int LoopDist;
	DWORD Address;

	char * Name[32];
	int * Team;
	int * Health;
	float * Position[3];
	float * AimbotAngle[3];

	char * OfflineName[32];
	int * OfflineTeam;
	int * OfflineHealth;
	float * OfflinePosition[3];
	float * OfflineAimbotAngle[3];

	//Read entity Health, Team Number, and Name
	void ReadInformation(int Player)
	{
		LoopDist = (Player * 0x4);

		Address = *(DWORD*)0x50F4F8 + LoopDist;

		if (Address != NULL)
		{
			Address = *(DWORD*)Address + 0x32C;

			if (Address != NULL)
			{
				Team = (int*)Address;
			}

			else
			{
				Team = NULL;
			}
		}

		else
		{
			Team = NULL;
		}
		
		Address = *(DWORD*)0x50F4F8 + LoopDist;

		if (Address != NULL)
		{
			Address = *(DWORD*)Address + 0xF8;

			if (Address != NULL)
			{
				Health = (int*)Address;
			}

			else
			{
				Health = NULL;
			}
		}

		else
		{
			Health = NULL;
		}

		LoopDist = Player * 0x4;

		//Read the entities team
		Address = *(DWORD*)0x510D90 + LoopDist;
		Address = *(DWORD*)Address + 0x32C;
		OfflineTeam = (int*)Address;

		//Read the entities health
		Address = *(DWORD*)0x510D90 + LoopDist;
		Address = *(DWORD*)Address + 0xF8;
		OfflineHealth = (int*)Address;

		//Read the entities name
		Address = *(DWORD*)0x510D90 + LoopDist;
		Address = *(DWORD*)Address + 0x225;
		for (int i = 0; i < 32; i++)
		{
			OfflineName[i] = (char*)Address + i;

			if (*OfflineName[i] == NULL)
				break;
		}
	}

	//Reads entity position
	void ReadPosition(int Player)
	{
		int j = 0;
		LoopDist = (Player * 0x4);

		for (int i = 0x4; i <= 0xC; i += 0x4)
		{
			Address = (DWORD)0x50F4F8 + LoopDist;
			if (Address != NULL)
			{
				Address = *(DWORD*)Address + i;

				if (Address != NULL)
				{
					Position[j] = (float*)Address;
				}

				else
				{
					Position[j] = NULL;
				}
			}

			else
			{
				Position[j] = NULL;
			}

			j++;
		}

		int h = 0;
		LoopDist = Player * 0x4;

		for (int i = 0x4; i <= 0xC; i += 0x4)
		{
			Address = *(DWORD*)0x510D90 + LoopDist;
			Address = *(DWORD*)Address + i;
			OfflinePosition[h] = (float*)Address;
			h++;
		}
	}
}PlayerList[64];

//This struct contains the information of OFFLINE entities
struct OfflinePlayerList_t
{
	float ThreeDdistance;
	float CrossDist;
	int LoopDist;
	DWORD Address;

	char * Name[32];
	int * Team;
	int * Health;
	float * Position[3];
	float * AimbotAngle[3];

	//Reads the entity's team, health, and name
	void ReadInformation(int Player)
	{
		LoopDist = Player * 0x4;

		//Read the entities team
		Address = *(DWORD*)0x510D90 + LoopDist;
		Address = *(DWORD*)Address + 0x32C;
		Team = (int*)Address;

		//Read the entities health
		Address = *(DWORD*)0x510D90 + LoopDist;
		Address = *(DWORD*)Address + 0xF8;
		Health = (int*)Address;

		//Read the entities name
		Address = *(DWORD*)0x510D90 + LoopDist;
		Address = *(DWORD*)Address + 0x225;
		for (int i = 0; i < 32; i++)
		{
			Name[i] = (char*)Address + i;

			if (*Name[i] == NULL)
				break;
		}
	}
	
	//Reads the entity's position
	void ReadPosition(int Player)
	{
		int j = 0;
		LoopDist = Player * 0x4;

		for (int i = 0x4; i <= 0xC; i += 0x4)
		{
		Address = *(DWORD*)0x510D90 + LoopDist;
		Address = *(DWORD*)Address + i;
		Position[j] = (float*)Address;
		j++;
		}
	}
}OfflinePlayerList[64];

//Return the 2D distance between you and an entity
float Get2dDistance(float Me[2], float You[2])
{
	return sqrt(((You[0] - Me[0]) * (You[0] - Me[0])) + ((You[1] - Me[1]) * (You[1] - Me[1])));
}

//Calculate the needed yaw and pitch for the aimbot
void CalcAngle(float src[2], float dst[2], float * angles)
{
	//I suck at math
	float deltaX = (src[0]) - (dst[0]);
	float deltaY = (src[1]) - (dst[1]);

	if ((dst[0]) > (src[0]) && (dst[1]) <= (src[1]))
	{
		//I know we do something with view angles here...
		angles[0] = atanf(deltaX / deltaY) * -180.0f / pi;
	}

	else if ((dst[0]) >= (src[0]) && (dst[1]) > (src[1]))
	{
		//And if that other something doesn't pass
		angles[0] = atanf(deltaX / deltaY) * -180.0f / pi + 180.0f;
		//we do something with view angles here...
	}

	else if ((dst[0]) < (src[0]) && (dst[1]) >= (src[1]))
	{
		angles[0] = atanf(deltaX / deltaY) * -180.0f / pi - 180.0f;
	}

	else if ((dst[0]) <= (src[0]) && (dst[1]) < (src[1]))
	{
		angles[0] = atanf(deltaX / deltaY) * -180.0f / pi + 360.0f;
		//Another case of the view angles
	}

	//Angle normalizing
	if (angles[0] < 0)
		angles[0] += 360;

	//Calcualting the pitch angle
	float deltaZ = (dst[2]) - (src[2]);
	float dist = sqrt(
		pow(double(dst[0] - src[0]), 2.0) +
		pow(double(dst[1] - src[1]), 2.0) +
		pow(double(dst[2] - src[2]), 2.0));

	//Pretty simple here
	angles[1] = asinf(deltaZ / dist) * 180.0f / pi;
}