#include "ReadingFunctions.h"
#include "ESP Vector Stuff.h"
#include <detours.h>
#include <gl/glut.h>
#include <string>
#include <conio.h>
#include <sstream>
#include <vector>
#include <algorithm>

bool SortType = true;
bool aimbot = false;
bool esp = false;
bool snapline = false;
bool Cross = false;
bool Radar = false;
bool Menu = false;
bool DistanceReading = false;
float InnerEdge = 10;
float Center = 160;
float OuterEdge = 310;
float Zoom = 1;

int TickCount = 0;

void DrawRadar();
void DrawMenu();
void Aimbot();
void ESP();
void Snapline();

float Ydraws[64];
float Xdraws[64];
float AfterDist[64];
float DrawX;
float DrawY;

struct CompareTargetArray
{
	//Credits to Fleep
	bool operator () (OfflinePlayerList_t lhs, OfflinePlayerList_t rhs)
	{
		return lhs.ThreeDdistance < rhs.ThreeDdistance;
	}
};

struct CompareTargetCrossArray
{
	//Credits to Fleep
	bool operator () (OfflinePlayerList_t lhs, OfflinePlayerList_t rhs)
	{
		return lhs.CrossDist < rhs.CrossDist;
	}
};

typedef void (WINAPI * _glVertex3f)(GLfloat x, GLfloat y, GLfloat z);

_glVertex3f __glVertex3f;

//glVertex3f Hook
void WINAPI ___glVertex3f(GLfloat x, GLfloat y, GLfloat z)
{

	(*__glVertex3f)(x, y, z);
}

typedef BOOL(WINAPI * _wglSwapBuffers) (_In_ HDC hDc);

_wglSwapBuffers __wglSwapBuffers;

//wglSwapBuffers Hook
BOOL WINAPI ___wglSwapBuffers(_In_ HDC hDc)
{
	if (esp)
	{
		ESP();
	}

	if (snapline)
	{
		Snapline();
	}

	//Call our drawing functions
	if (Radar)
	{
		DrawRadar();
	}

	if (Menu)
	{
		DrawMenu();
	}

	return __wglSwapBuffers(hDc);
}

void ESP()
{
	__asm pushfd
	__asm pushad

	for (int i = 0; i < *MyPlayer.OfflineNumOfPlayers; i++)
	{
		glmatrixf *glmvpmatrix = (glmatrixf*)(0x501AE8);
		vec Position(*OfflinePlayerList[i].Position[0], *OfflinePlayerList[i].Position[1], *OfflinePlayerList[i].Position[2]);

		if (*OfflinePlayerList[i].Health <= 0 || *OfflinePlayerList[i].Health > 100)
			continue;

		if (glmvpmatrix->transformw(Position) < 0.0f)
			continue;

		else
		{
			glPushMatrix();

			//Grab the windows information and assign it to some variables
			//The we will use for drawing
			windowz.GrabInfo();
			std::vector<int> WidthHeight(2);

			WidthHeight.push_back(*windowz.Height);
			WidthHeight.push_back(*windowz.Width);

			float Width = *windowz.Width;
			float Height = *windowz.Height;

			//Make sure we're drawing in an area of 800x600
			glViewport(0, 0, Width, Height);
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			glOrtho(0, Width, Height, 0, -1, 1);
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();

			vec TransformedPosition = math2.Transform(glmvpmatrix, Position);
			TransformedPosition.x *= Width / 2.0f;
			TransformedPosition.y *= -Height / 2.0f;
			TransformedPosition.x += Width / 2.0f;
			TransformedPosition.y += Height / 2.0f;

			float PlayerDist = Get3dDistance(*MyPlayer.Position, *OfflinePlayerList[i].Position);

			glLineWidth(1.0);

			if ((*MyPlayer.GameMode == 0 || *MyPlayer.GameMode == 4 || *MyPlayer.GameMode == 5 || *MyPlayer.GameMode == 7 || *MyPlayer.GameMode == 13 || *MyPlayer.GameMode == 11 || *MyPlayer.GameMode == 14 || *MyPlayer.GameMode == 17 || *MyPlayer.GameMode == 16 || *MyPlayer.GameMode == 20 || *MyPlayer.GameMode == 21))
			{
				if (*MyPlayer.Team == *OfflinePlayerList[i].Team)
					glColor3f(0.0, 255.0, 0.0);

				else
					glColor3f(255.0, 0.0, 0.0);
			}

			else
				glColor3f(255.0, 0.0, 0.0);

			glEnable(GL_LINE_SMOOTH);

			glBegin(GL_LINE_LOOP);
			glVertex3f(TransformedPosition.x - 400 / PlayerDist, TransformedPosition.y - 300 / PlayerDist, 0.0);
			glVertex3f(TransformedPosition.x + 400 / PlayerDist, TransformedPosition.y - 300 / PlayerDist, 0.0);
			glVertex3f(TransformedPosition.x + 400 / PlayerDist, TransformedPosition.y + 1900 / PlayerDist, 0.0);
			glVertex3f(TransformedPosition.x - 400 / PlayerDist, TransformedPosition.y + 1900 / PlayerDist, 0.0);
			glEnd();

			glDisable(GL_LINE_SMOOTH);

			glPopMatrix();
		}
	}
	__asm popad
	__asm popfd
}

void Snapline()
{
	__asm pushfd
	__asm pushad

	for (int i = 0; i < *MyPlayer.OfflineNumOfPlayers; i++)
	{
		glmatrixf *glmvpmatrix = (glmatrixf*)(0x501AE8);
		vec Position(*OfflinePlayerList[i].Position[0], *OfflinePlayerList[i].Position[1], *OfflinePlayerList[i].Position[2]);

		if (*OfflinePlayerList[i].Health <= 0 || *OfflinePlayerList[i].Health > 100)
			continue;

		if (glmvpmatrix->transformw(Position) < 0.0f)
			continue;

		else
		{
			glPushMatrix();

			//Grab the windows information and assign it to some variables
			//The we will use for drawing
			windowz.GrabInfo();
			std::vector<int> WidthHeight(2);

			WidthHeight.push_back(*windowz.Height);
			WidthHeight.push_back(*windowz.Width);

			float Width = *windowz.Width;
			float Height = *windowz.Height;

			//Make sure we're drawing in an area of 800x600
			glViewport(0, 0, Width, Height);
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			glOrtho(0, Width, Height, 0, -1, 1);
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();

			vec TransformedPosition = math2.Transform(glmvpmatrix, Position);
			TransformedPosition.x *= Width / 2.0f;
			TransformedPosition.y *= -Height / 2.0f;
			TransformedPosition.x += Width / 2.0f;
			TransformedPosition.y += Height / 2.0f;

			float PlayerDist = Get3dDistance(*MyPlayer.Position, *OfflinePlayerList[i].Position);

			glLineWidth(1.0);

			if ((*MyPlayer.GameMode == 0 || *MyPlayer.GameMode == 4 || *MyPlayer.GameMode == 5 || *MyPlayer.GameMode == 7 || *MyPlayer.GameMode == 13 || *MyPlayer.GameMode == 11 || *MyPlayer.GameMode == 14 || *MyPlayer.GameMode == 17 || *MyPlayer.GameMode == 16 || *MyPlayer.GameMode == 20 || *MyPlayer.GameMode == 21))
			{
				if (*MyPlayer.Team == *OfflinePlayerList[i].Team)
					glColor3f(0.0, 255.0, 0.0);

				else
					glColor3f(255.0, 0.0, 0.0);
			}

			else
				glColor3f(255.0, 0.0, 0.0);

			glEnable(GL_LINE_SMOOTH);

			glBegin(GL_LINES);
			glVertex3f(Width / 2, Height, 0.0);
			glVertex3f(TransformedPosition.x, TransformedPosition.y + 1900 / PlayerDist, 0.0);
			glEnd();

			glDisable(GL_LINE_SMOOTH);

			glPopMatrix();
		}
	}
	__asm popad
	__asm popfd
}

//Reads player's info, calculates angles, then aims at the enemy
void Aimbot()
{
	//Read the player's information to make sure that
	//we don't dereference a NULL health pointer
	//MyPlayer.ReadInformation();
	//MyPlayer.ReadPosition();

	if ((*MyPlayer.Health > 0 || *MyPlayer.Health < 100))
	{
		//If right mouse button
		if (GetAsyncKeyState(0x02))
		{

			//Loop through the players
			for (int i = 0; i < *MyPlayer.OfflineNumOfPlayers; i++)
			{
				//Read the playerlist information
				//OfflinePlayerList[i].ReadInformation(i);
				//OfflinePlayerList[i].ReadPosition(i);

				//Make SURE the ones that are skipped don't get put first in the sort list
				OfflinePlayerList[i].ThreeDdistance = 1000000;
				OfflinePlayerList[i].CrossDist = 1000000;

				//Filter out the ones that are dead
				if (*OfflinePlayerList[i].Health <= 0 || *OfflinePlayerList[i].Health > 100)
					continue;

				//If it's a team game
				if ((*MyPlayer.GameMode == 0 || *MyPlayer.GameMode == 4 || *MyPlayer.GameMode == 5 || *MyPlayer.GameMode == 7 || *MyPlayer.GameMode == 13 || *MyPlayer.GameMode == 11 || *MyPlayer.GameMode == 14 || *MyPlayer.GameMode == 17 || *MyPlayer.GameMode == 16 || *MyPlayer.GameMode == 20 || *MyPlayer.GameMode == 21))
				{
					//Skip if they're on our team
					if (*OfflinePlayerList[i].Team == *MyPlayer.Team)
						continue;
				}

				float CalcAngles[2];
				CalcAngle(*MyPlayer.Position, *OfflinePlayerList[i].Position, CalcAngles);
				//Now check for the distance
				OfflinePlayerList[i].ThreeDdistance = Get3dDistance(*MyPlayer.Position, *OfflinePlayerList[i].Position);
				OfflinePlayerList[i].CrossDist = std::fabs((CalcAngles[0] - *MyPlayer.ViewAngles[0]) + (CalcAngles[1] - *MyPlayer.ViewAngles[1]));
			}

			//Doin' some sorting here
			if (SortType)
				std::sort(OfflinePlayerList, OfflinePlayerList + *MyPlayer.OfflineNumOfPlayers, CompareTargetArray());

			else
				std::sort(OfflinePlayerList, OfflinePlayerList + *MyPlayer.OfflineNumOfPlayers, CompareTargetCrossArray());

			float AimbotAngles[2];
			CalcAngle(*MyPlayer.Position, *OfflinePlayerList[0].Position, AimbotAngles);

			//Aim at them.
			*MyPlayer.ViewAngles[0] = AimbotAngles[0];
			//Make SURE we aim at them
			*MyPlayer.ViewAngles[1] = AimbotAngles[1];
		}
	}
}

//Calculates the position of every player,
//then draws them onto the radar
void DrawRadar()
{
	//Read our information
	//MyPlayer.ReadInformation();
	//MyPlayer.ReadPosition();

	//Need these declared here for the distance drawing function
	float Ydraws[64];
	float Xdraws[64];
	float AfterDist[64];
	float DrawX;
	float DrawY;

	for (int i = 0; i < *MyPlayer.OfflineNumOfPlayers - 1; i++)
	{
		//OfflinePlayerList[i].ReadInformation(i);
		//OfflinePlayerList[i].ReadPosition(i);

		//Skip if they're dead
		if (*OfflinePlayerList[i].Health <= 0 || *OfflinePlayerList[i].Health > 100)
			continue;

		float DoMathX;
		float DoMathY;

		float DrawXY[2];

		float PlayerDrawX = *MyPlayer.Position[0];
		float PlayerDrawY = *MyPlayer.Position[1];

		float EnemyDrawX = *OfflinePlayerList[i].Position[0];
		float EnemyDrawY = *OfflinePlayerList[i].Position[1];
		
		//So they aren't on top of us
		if (EnemyDrawX == PlayerDrawX)
		{
			EnemyDrawX += 0.0001;
		}

		if (EnemyDrawY == PlayerDrawY)
		{
			EnemyDrawY += 0.0001;
		}

		//Start doing some math stuff here
		//*STUDY THIS TILL YOU UNDERSTAND IT*
		float delta_x = abs(EnemyDrawX - PlayerDrawX);
		float delta_y = abs(EnemyDrawY - PlayerDrawY);
		float Dist = pow(((pow(delta_x, 2)) + (pow(delta_y, 2))), 0.5);
		float yaw_id = atan(delta_y / delta_x) * 57.295779513082323;

		if (EnemyDrawX < PlayerDrawX && EnemyDrawY > PlayerDrawY)
		{
			yaw_id = 180 - yaw_id;
		}

		else
		{
			if (EnemyDrawX < PlayerDrawX && EnemyDrawY < PlayerDrawY)
			{
				yaw_id = -180 + yaw_id;
			}
			else
			{
				if (EnemyDrawX > PlayerDrawX && EnemyDrawY < PlayerDrawY)
				{
					yaw_id = yaw_id * -1;
				}
			}
		}

		float yaw_delta = *MyPlayer.ViewAngles[0] - yaw_id + 90;
		float temp_id_x = (sin(yaw_delta / 57.295779513082323) * Dist);
		float temp_id_y = (cos(yaw_delta / 57.295779513082323) * Dist);

		//Assign the position to the radar
		DrawX = Center + temp_id_x * Zoom;
		DrawY = Center + temp_id_y * Zoom;

		//If it's a team game
		if ((*MyPlayer.GameMode == 0 || *MyPlayer.GameMode == 4 || *MyPlayer.GameMode == 5 || *MyPlayer.GameMode == 7 || *MyPlayer.GameMode == 13 || *MyPlayer.GameMode == 11 || *MyPlayer.GameMode == 14 || *MyPlayer.GameMode == 17 || *MyPlayer.GameMode == 16 || *MyPlayer.GameMode == 20 || *MyPlayer.GameMode == 21))
		{
			//If they're on our team, draw as green
			if (*OfflinePlayerList[i].Team == *MyPlayer.Team)
			{
				//Make sure it isn't off the radar
				if (DrawX >= OuterEdge || DrawX <= InnerEdge || DrawY >= OuterEdge || DrawY <= InnerEdge)
				{
					if (DrawX >= OuterEdge)
						DrawX = OuterEdge;

					if (DrawY >= OuterEdge)
						DrawY = OuterEdge;

					if (DrawX <= InnerEdge)
						DrawX = InnerEdge;

					if (DrawY <= InnerEdge)
						DrawY = InnerEdge;

					glPointSize(4);
					glColor3f(0.8f, 0.8f, 0.8f);

					glBegin(GL_POINTS);
					glVertex2f(DrawX, DrawY);
					glEnd();
				}

				else
				{
					//Draw the green dot
					glPointSize(4);
					glColor3f(0.0f, 1.0f, 0.0f);

					glBegin(GL_POINTS);
					glVertex2f(DrawX, DrawY);
					glEnd();
				}
			}

			//If they aren't on our team
			else if (*OfflinePlayerList[i].Team != *MyPlayer.Team)
			{
				//Make sure they aren't off the radar
				if (DrawX >= OuterEdge || DrawX <= InnerEdge || DrawY >= OuterEdge || DrawY <= InnerEdge)
				{
					if (DrawX >= OuterEdge)
						DrawX = OuterEdge;

					if (DrawY >= OuterEdge)
						DrawY = OuterEdge;

					if (DrawX <= InnerEdge)
						DrawX = InnerEdge;

					if (DrawY <= InnerEdge)
						DrawY = InnerEdge;

					glPointSize(4);
					glColor3f(0.8f, 0.8f, 0.8f);

					glBegin(GL_POINTS);
					glVertex2f(DrawX, DrawY);
					glEnd();
				}

				//Draw the red dot
				glPointSize(4);
				glColor3f(1.0f, 0.0f, 0.0f);

				glBegin(GL_POINTS);
				glVertex2f(DrawX, DrawY);
				glEnd();
			}
		}

		//If it isn't a team game
		else
		{
			//Make sure they aren't off the radar
			if (DrawX >= OuterEdge || DrawX <= InnerEdge || DrawY >= OuterEdge || DrawY <= InnerEdge)
			{
				if (DrawX >= OuterEdge)
					DrawX = OuterEdge;

				if (DrawY >= OuterEdge)
					DrawY = OuterEdge;

				if (DrawX <= InnerEdge)
					DrawX = InnerEdge;

				if (DrawY <= InnerEdge)
					DrawY = InnerEdge;

				glPointSize(4);
				glColor3f(0.8f, 0.8f, 0.8f);

				glBegin(GL_POINTS);
				glVertex2f(DrawX, DrawY);
				glEnd();
			}

			//Draw a red dot
			glPointSize(4);
			glColor3f(1.0f, 0.0f, 0.0f);

			glBegin(GL_POINTS);
			glVertex2f(DrawX, DrawY);
			glEnd();
		}

		//Initializing these for the draw distance function
		Xdraws[i] = DrawX;
		Ydraws[i] = DrawY;
		AfterDist[i] = Get2dDistance(*MyPlayer.Position, *OfflinePlayerList[i].Position);
	}
	//Draw the center dot of the radar
	glPointSize(4);

	glColor3f(0.7f, 0.7f, 0.7f);

	glBegin(GL_POINTS);
	glVertex2f(Center, Center);
	glEnd();

	glColor3f(1.0f, 1.0f, 1.0f);
	glLineWidth(2.0);

	//Draw the radar cross
	glBegin(GL_LINES);
	glVertex2f(Center, InnerEdge);
	glVertex2f(Center, OuterEdge);

	glVertex2f(InnerEdge, Center);
	glVertex2f(OuterEdge, Center);
	glEnd();

	glColor3f(0.2f, 0.2f, 0.2f);

	//Draw the radar box
	glBegin(GL_POLYGON);
	glVertex2f(InnerEdge, InnerEdge);
	glVertex2f(OuterEdge, InnerEdge);
	glVertex2f(OuterEdge, OuterEdge);
	glVertex2f(InnerEdge, OuterEdge);
	glEnd();

	if (DistanceReading)
	{
		//So that the text shows in front
		glDisable(GL_DEPTH_TEST);

		for (int i = 0; i < *MyPlayer.OfflineNumOfPlayers; i++)
		{
			//Set the position on the radar for drawing
			glColor3f(1.0f, 1.0f, 1.0f);
			glRasterPos2i(Xdraws[i] - 33, Ydraws[i] + 18);
			void * font = GLUT_BITMAP_HELVETICA_10;

			std::ostringstream oss;
			oss << AfterDist[i];
			std::string Dist = "(" + oss.str() + ")";

			for (std::string::iterator j = Dist.begin(); j != Dist.end(); ++j)
			{
				char c = *j;
				glutBitmapCharacter(font, c);
			}
		}

		glEnable(GL_DEPTH_TEST);
	}
}

//Draws the hack menu
void DrawMenu()
{
	glPushMatrix();

	//Grab the windows information and assign it to some variables
	//The we will use for drawing
	windowz.GrabInfo();
	std::vector<int> WidthHeight(2);

	WidthHeight.push_back(*windowz.Height);
	WidthHeight.push_back(*windowz.Width);

	float Width = *windowz.Width;
	float Height = *windowz.Height;
	
	//Make sure we're drawing in an area of 800x600
	glViewport(0, 0, Width, Height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, Width, Height, 0, -1, 1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glColor3f(1.0f, 1.0f, 1.0f);
	glLineWidth(3.0);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_BLEND);

	//Menu outline
	glBegin(GL_LINE_LOOP);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex2f(Width / 2 - 124, Height / 2 - 174);
	glVertex2f(Width / 2 + 124, Height / 2 - 174);
	glColor3f(1.0f, 0.68f, 0.78f);
	glVertex2f(Width / 2 + 124, Height / 2 + 174);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex2f(Width / 2 - 124, Height / 2 + 174);
	glEnd();

	glDisable(GL_BLEND);
	glDisable(GL_LINE_SMOOTH);

	glEnable(GL_BLEND);

	//Menu
	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.68f, 0.78f);
	glVertex2f(Width / 2 - 125, Height / 2 - 175);
	glVertex2f(Width / 2 + 125, Height / 2 - 175);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex2f(Width / 2 + 125, Height / 2 + 175);
	glColor3f(1.0f, 0.68f, 0.78f);
	glVertex2f(Width / 2 - 125, Height / 2 + 175);
	glEnd();

	glDisable(GL_BLEND);

	glDisable(GL_DEPTH_TEST);

	//Start drawing the words on the menu
	int RasterHeight = Height / 2 - 155;
	int RasterWidth = Width / 2 - 120;

	glColor3f(0.85f, 0.2f, 0.67f);
	glRasterPos2i(RasterWidth, RasterHeight);
	void * font = GLUT_BITMAP_HELVETICA_18;

	std::string RadarBool;
	std::string DistanceBool;
	std::string AimbotBool;
	std::string SortTypeBool;
	std::string ESPBool;
	std::string SnaplineBool;

	std::string RadarClaim;
	std::string DistanceClaim;
	std::string AimbotClaim;
	std::string SortTypeClaim;
	std::string ESPClaim;
	std::string SnaplineClaim;

	if (Radar)
		RadarBool = "On";

	else
		RadarBool = "Off";

	if (DistanceReading)
		DistanceBool = "On";

	else
		DistanceBool = "Off";

	if (aimbot)
		AimbotBool = "On";

	else
		AimbotBool = "Off";

	if (esp)
		ESPBool = "On";

	else
		ESPBool = "Off";

	if (snapline)
		SnaplineBool = "On";

	else
		SnaplineBool = "Off";

	if (SortType)
		SortTypeBool = "Player";

	else
		SortTypeBool = "Crosshair";

	DistanceClaim = "Distance Meter: ";
	AimbotClaim = "Aimbot: ";
	SortTypeClaim = "Sort By ";
	ESPClaim = "ESP: ";
	SnaplineClaim = "Snapline: ";
	RadarClaim = "Radar: " + RadarBool + "\n" + DistanceClaim + DistanceBool + "\n" + AimbotClaim + AimbotBool + "\n" + SortTypeClaim + SortTypeBool + " Distance" + "\n" + ESPClaim + ESPBool + "\n" + SnaplineClaim + SnaplineBool + "\n";

	std::string RadarSize;
	std::string RadarZoom;
	int OriginalRadarSize = 160;
	int OriginalRadarZoom = 1;
	int RadarSizeMath = Center / OriginalRadarSize * 100;
	int RadarZoomMath = Zoom / OriginalRadarZoom * 100;

	std::ostringstream oss;
	oss << RadarSizeMath;
	RadarSize = "Radar Size: " + oss.str() + "%";

	std::ostringstream iss;
	iss << RadarZoomMath;
	RadarZoom = "Radar Zoom: " + iss.str() + "%";

	RadarClaim = RadarClaim + "\n" + RadarSize + "\n" + RadarZoom;

	std::string Credits = "\n\n\n\n\n\n\n\nCredits to Spock, GAFO666,\nand AnomanderRake!";

	RadarClaim = RadarClaim + Credits;

	for (std::string::iterator j = RadarClaim.begin(); j != RadarClaim.end(); ++j)
	{
		char c = *j;
		
		if (c == '\n')
		{
			RasterHeight += 19;
			glRasterPos2i(RasterWidth, RasterHeight);
		}

		glutBitmapCharacter(font, c);
	}

	glEnable(GL_DEPTH_TEST);

	glPopMatrix();
}

//Detours the OpenGL SwapBuffers function
void HookSwapBuffers()
{
	HMODULE hMod = GetModuleHandle("opengl32.dll");

	if (hMod)
	{
		__wglSwapBuffers = (_wglSwapBuffers)(DWORD)GetProcAddress(hMod, "wglSwapBuffers");
		DetourTransactionBegin();
		DetourUpdateThread(GetCurrentThread());
		DetourAttach(&(PVOID &)__wglSwapBuffers, ___wglSwapBuffers);
		DetourTransactionCommit();
	}
}

//Detours the OpenGL glVertex3f function
void HookglVertex3f()
{
	HMODULE hMod = GetModuleHandle("opengl32.dll");

	if (hMod)
	{
		__glVertex3f = (_glVertex3f)(DWORD)GetProcAddress(hMod, "glVertex3f");
		DetourTransactionBegin();
		DetourUpdateThread(GetCurrentThread());
		DetourAttach(&(PVOID &)__glVertex3f, ___glVertex3f);
		DetourTransactionCommit();
	}
}

void DetourglEnd()
{

}