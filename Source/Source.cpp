#include "Functions.h"

//Main thread
DWORD WINAPI QHook_Main(LPVOID param)
{
	//Call our swapbuffers hook only once
	HookSwapBuffers();

	//This is so we only read every once in a while
	int ReadSwitch = 0;

	while (true)
	{
		if (ReadSwitch == 0)
		{
			MyPlayer.ReadInformation();
			MyPlayer.ReadPosition();

			for (int i = 0; i < *MyPlayer.OfflineNumOfPlayers; i++)
			{
				PlayerList[i].ReadInformation(i);
				PlayerList[i].ReadPosition(i);

				OfflinePlayerList[i].ReadInformation(i);
				OfflinePlayerList[i].ReadPosition(i);
			}
		}

		ReadSwitch++;

		if (ReadSwitch == 1000)
			ReadSwitch = 0;

		//Toggles for the hack features
		if (GetAsyncKeyState(VK_NUMPAD1) & 1)
		{
			Radar = !Radar;
		}

		if (GetAsyncKeyState(VK_NUMPAD2) & 1)
		{
			DistanceReading = !DistanceReading;
		}

		if (GetAsyncKeyState(VK_NUMPAD3) & 1)
		{
			aimbot = !aimbot;
		}

		if (GetAsyncKeyState(VK_NUMPAD4) & 1)
		{
			SortType = !SortType;
		}

		if (GetAsyncKeyState(VK_NUMPAD5) & 1)
		{
			esp = !esp;
		}

		if (GetAsyncKeyState(VK_NUMPAD6) & 1)
		{
			snapline = !snapline;
		}

		if (GetAsyncKeyState(VK_NUMPAD7) & 1)
		{
			Menu = !Menu;
		}

		//Adjustments for the radar
		if (GetAsyncKeyState(VK_UP) & 1 && Radar)
		{
			Center += 5;
			OuterEdge += 10;
		}

		if (GetAsyncKeyState(VK_DOWN) & 1 && Radar)
		{
			Center -= 5;
			OuterEdge -= 10;
		}

		if (GetAsyncKeyState(VK_LEFT) & 1 && Radar)
		{
			if (Zoom > 0.5)
				Zoom -= 0.1;
		}

		if (GetAsyncKeyState(VK_RIGHT) & 1 && Radar)
		{
			if (Zoom < 1.5)
				Zoom += 0.1;
		}

		//Calling the aimbot function
		if (aimbot)
			Aimbot();

		//Freeing the DLL
		if (GetAsyncKeyState(VK_NUMPAD9) & 1)
		{
			HMODULE myModule = GetModuleHandleA("AssaultCube Radar.dll");
			FreeLibraryAndExitThread(myModule, NULL);
		}
	}

	return NULL;
}

//Our entry point
BOOL WINAPI DllMain(HINSTANCE instance, DWORD reason, LPVOID reserved) {
	switch (reason) {
	case DLL_PROCESS_ATTACH:
		CreateThread(0, 0, QHook_Main, 0, 0, 0);
		DisableThreadLibraryCalls(instance);
		AllocConsole();
		AttachConsole(GetProcessId(instance));
		break;
	case DLL_THREAD_ATTACH:
	case DLL_PROCESS_DETACH:
	case DLL_THREAD_DETACH:
		FreeConsole();
		break;
	}

	return TRUE;
}